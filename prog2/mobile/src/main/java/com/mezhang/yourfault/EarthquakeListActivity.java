package com.mezhang.yourfault;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;



public class EarthquakeListActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    ListView listView ;
    List<EarthquakeData> results = new ArrayList<>();
    // Defined Array values to show in ListView
    List<String> values = new ArrayList<String>();
    ArrayAdapter<String> adapter = null;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    double mylong=37.8716;
    double mylat=-122.273;

    private static Random random = new Random();

    private class EarthquakeData {
        long time;
        double magnitude;
        double longitude;
        double latitude;
        String place;
    }

    private class MyTask extends TimerTask {
        public void run(){
            getEarthquakeData();
            convert();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthquake_list);
        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.list);
        Timer myTimer = new Timer("MyTimer", true);
        myTimer.scheduleAtFixedRate(new MyTask(), 0, 10000000);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);



        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                clickItem(position);
            }
        });

        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi( Wearable.API )
                .build();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        Log.d("DEBUG", LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient).toString());
        if (mLastLocation == null) {
            mylat = 37.8716;
            mylong = -122.273;
        }
        else {
            mylat = mLastLocation.getLatitude();
            mylong = mLastLocation.getLongitude();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private static double calculate_distance(double latitude, double longitude, double mylat, double mylong){
        int R = 6371000; // metres
        double latitude_ra = Math.toRadians(latitude);
        double mylat_ra = Math.toRadians(mylat);
        double diff_ra = Math.toRadians(mylat-latitude);
        double diff_long_ra = Math.toRadians(mylong-longitude);
        double a = Math.sin(diff_ra/2) * Math.sin(diff_ra/2) +
                Math.cos(latitude_ra) * Math.cos(mylat_ra)* Math.sin(diff_long_ra/2) * Math.sin(diff_long_ra/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double b = R * c;
        return b;
    }

    private void clickItem(int position) {
        EarthquakeData data = results.get(position);
        // Launching new Activity on selecting single List Item
        Intent i = getMapIntent(data);
        startActivity(i);
        notify_watch(data);
    }

    private void convert(){
        values.clear();
        for (EarthquakeData i:results){
            Log.d("debug",i.place);
            String earthquake = "";
            earthquake += i.magnitude;
            earthquake += "M";
            earthquake += "  ";
            earthquake += i.place;
            values.add(earthquake);
        }

        for (String i:values){
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }


    private void getEarthquakeData() {
        HttpURLConnection urlConnection = null;
        ArrayList<EarthquakeData> newResults = new ArrayList<>();
        try {
            URL url = new URL("http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_week.geojson");
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            String data = s.hasNext() ? s.next() : "";
            JSONObject json = new JSONObject(data);
            JSONArray features = json.getJSONArray("features");
            //Log.d("DEBUG", "features.length() = " + features.length());
            for (int i = 0; i < features.length(); i++) {
                JSONObject feature = features.getJSONObject(i);
                EarthquakeData result = new EarthquakeData();
                JSONObject properties = feature.getJSONObject("properties");
                result.magnitude = properties.getDouble("mag");
                String longPlace = properties.getString("place");
                String[] arr = longPlace.split(" of ");
                Log.d("DEBUG", "arr[0]:" + arr[0]);
                String place = null;
                if (arr.length == 1) {
                    place = arr[0];
                } else {
                    place = arr[1];
                }
                result.place = place;
                result.time = properties.getLong("time");
                JSONObject geometry = feature.getJSONObject("geometry");
                JSONArray coordinates = geometry.getJSONArray("coordinates");
                result.longitude = coordinates.getDouble(0);
                result.latitude = coordinates.getDouble(1);
                newResults.add(result);
            }
        } catch (Exception e) {
        } finally {
            urlConnection.disconnect();
        }
        if (results == null || results.size() == 0){
            results = newResults;
            /*
            if getIntent().getIntExtra("No",0) == 0) {
                clickItem(0);
            }
            */
        }
        else if (results.get(0).equals(newResults.get(0))) {

        } else {
            results = newResults;
            clickItem(0);

        }

    }

    private Intent getMapIntent(EarthquakeData data) {
        Intent viewIntent = new Intent(this, EarthquakeMap.class);
        viewIntent.putExtra("longitude", data.longitude);
        viewIntent.putExtra("latitude",data.latitude);
        viewIntent.putExtra("place",data.place);
        viewIntent.putExtra("mag", data.magnitude);

        double distanceMeter = calculate_distance(data.latitude, data.longitude, mylat, mylong);
        int distanceKM = (int) (distanceMeter / 1000);
        viewIntent.putExtra("dis", distanceKM);
        return viewIntent;
    }


    private void notify_watch(EarthquakeData data){
        double mlongitude = data.longitude;
        double mlatitude = data.latitude;
        String mplace = data.place;
        double mmage = data.magnitude;
        double distanceMeter = calculate_distance(data.latitude, data.longitude, mylat, mylong);
        int distanceKM = (int) (distanceMeter / 1000);

        sendMessage("/EarthquakeShow", "{" + "longitude:" + -122 + ", " + "latitude:"+
        37 + ", " + "place:" +  "\"" + mplace + "\"" + ", " + "magnitude:" + mmage
        +", " + "distance:" + distanceKM + "}");
    }

    private void sendMessage( final String path, final String text ) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mGoogleApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient, node.getId(), path, text.getBytes() ).await();
                }


            }
        }).start();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }
}

