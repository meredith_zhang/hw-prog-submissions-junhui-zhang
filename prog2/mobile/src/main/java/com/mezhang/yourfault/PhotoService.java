package com.mezhang.yourfault;

import android.content.Intent;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

public class PhotoService extends WearableListenerService {
    int count = 0;
    private static final String RECEIVER_SERVICE_PATH = "/photo";
    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase( RECEIVER_SERVICE_PATH ) ) {
            String data =new String( messageEvent.getData());
            Intent intent = new Intent( this,LoadPhotoActivity.class );
            intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("place",data);
            startActivity(intent);
            count ++;
        } else if ( messageEvent.getPath().equalsIgnoreCase("/stop")) {
            Intent intent = new Intent(this,EarthquakeListActivity.class);
            intent.putExtra("No",1);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {

            super.onMessageReceived( messageEvent );
        }
    }
}

