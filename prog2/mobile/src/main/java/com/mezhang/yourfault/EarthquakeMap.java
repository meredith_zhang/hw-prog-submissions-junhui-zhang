package com.mezhang.yourfault;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class EarthquakeMap extends FragmentActivity implements
        OnMapReadyCallback {

    private MapFragment mapFragment;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    double longitude = 0;
    double latitude = 0;
    double mag = 0;
    String place = "";
    int dis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        longitude = intent.getDoubleExtra("longitude", 0);
        latitude = intent.getDoubleExtra("latitude", 0);
        mag =  intent.getDoubleExtra("mag", 0) ;
        place = intent.getStringExtra("place");
        dis = intent.getIntExtra("dis", 0);
        setContentView(R.layout.activity_earthquake_map);
        setUpMapIfNeeded();

        //Log.d("DEBUG", "Finished initializing location service");

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("Marker"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude),10));
        ActionBar actionBar = getActionBar();
        actionBar.setTitle("Earthquake" + " @ " + place);
        actionBar.setSubtitle(mag + "M          " + dis + " km");
    }
}
