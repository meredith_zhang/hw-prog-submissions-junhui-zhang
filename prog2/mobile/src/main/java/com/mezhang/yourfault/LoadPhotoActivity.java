package com.mezhang.yourfault;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class LoadPhotoActivity extends Activity {
    double longitude = 0;
    double latitude = 0;
    int count = 0;
    String place = null;
    double lat;
    double longi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_photo);
        place = getIntent().getStringExtra("place");
        this.setTitle(place);
        LoadPhoto();
    }

    private void getgeo(){
        String location=place;
        Log.d("DEBUG","place : " + place);
        location=location.replaceAll(" ", "%20");
        String myUrl="http://maps.googleapis.com/maps/api/geocode/" +
                "json?address=" +
                location +
                "&sensor=false";
        try{
            URL url=new URL(myUrl);
            URLConnection urlConnection=url.openConnection();
            BufferedReader in = new BufferedReader(new
                    InputStreamReader(urlConnection.getInputStream()));
            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            String data = s.hasNext() ? s.next() : "";
            JSONObject json = new JSONObject(data);

            JSONArray results = json.getJSONArray("results");
            JSONObject result = results.getJSONObject(0);
            JSONObject geo = result.getJSONObject("geometry");
            JSONObject mylocation = geo.getJSONObject("location");
            lat = mylocation.getDouble("lat");
            longi = mylocation.getDouble("lng");


            Log.d("DEBUG","lat" + lat);
            Log.d("DEBUG","longi" + longi);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }


    @Override
    protected  void onNewIntent(Intent intent){
        count ++;
        new Thread(new Runnable() {
            @Override public void run() {
                String urlString = GetUrl().get(count);
                new DownloadImageTask((ImageView) findViewById(R.id.immageViewmageView))
                        .execute(urlString);
            }
        }).start();
    }


    private void LoadPhoto(){

        new Thread(new Runnable() {
            @Override public void run() {
                getgeo();
                try {
                    String urlString = GetUrl().get(0);
                    new DownloadImageTask((ImageView) findViewById(R.id.immageViewmageView))
                            .execute(urlString);
                }catch (Exception e){
                    String urlString = "http://7-themes.com/data_images/out/33/6882125-smile.jpg";
                    new DownloadImageTask((ImageView) findViewById(R.id.immageViewmageView))
                            .execute(urlString);
                }

            }
        }).start();
    }

    private ArrayList<String> GetUrl(){
        String InstagramRequest = "https://api.instagram.com/v1/media/search?lat=" + lat
                + "&lng=" + longi + "&client_id=" + "63c09aface47489891998a02df30f0c8" + "&distance=5000";
        ArrayList<String> imageUrls = new ArrayList<>();
        try {
            URL url = new URL(InstagramRequest);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            String data = s.hasNext() ? s.next() : "";
            JSONObject json = new JSONObject(data);
            JSONArray items = json.getJSONArray("data");
            for (int i = 0; i < items.length(); i++){
                JSONObject item = items.getJSONObject(i);
                if (item.getString("type").equals("image")){
                    JSONObject images = item.getJSONObject("images");
                    if (images.has("standard_resolution")) {
                        JSONObject image = images.getJSONObject("standard_resolution");
                        imageUrls.add(image.getString("url"));
                    }
                }
            }

        }catch (Exception e){

        }
        for (String urlString : imageUrls) {
            Log.d("DEBUG", "Image url: " + urlString);
        }
        return imageUrls;
    }



private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        if (urldisplay == null) {

        }
        else{

        }
        Bitmap mIcon11 = null;
        try {
            URL urln = new URL(urldisplay);
            //InputStream in = urln.openConnection().getInputStream();
            HttpURLConnection con = (HttpURLConnection)urln.openConnection();
            InputStream in = con.getInputStream();
            mIcon11 = BitmapFactory.decodeStream(in);


        } catch (Exception e) {
            Log.e("Error", e.getClass().getCanonicalName());
            e.printStackTrace();
        }
        final Bitmap bm = mIcon11;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bmImage.setImageBitmap(bm);
                Log.d("DEBUG", "ImageView " + bmImage.getWidth() + "*" + bmImage.getHeight());
            }
        });

        return mIcon11;
    }

}
}
