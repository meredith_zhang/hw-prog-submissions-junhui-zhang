package com.mezhang.yourfault;

import android.content.Intent;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import org.json.JSONObject;

public class EarthquakeListenerService extends WearableListenerService {
    public EarthquakeListenerService() {
    }
    private static final String START_ACTIVITY = "/EarthquakeShow";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase( START_ACTIVITY ) ) {
            String message = new String(messageEvent.getData());
            try {
                JSONObject json = new JSONObject(message);
                String place = json.getString("place");
                double longitude = json.getDouble("longitude");
                double latitude = json.getDouble("latitude");
                double magnitude = json.getDouble("magnitude");
                int distance = json.getInt("distance");

                Intent intent = new Intent(this, EarthquakeShow.class);
                intent.putExtra("longitude", longitude);
                intent.putExtra("latitude", latitude);
                intent.putExtra("place", place);
                intent.putExtra("magnitude", magnitude);
                intent.putExtra("distance", distance);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            super.onMessageReceived( messageEvent );
        }
    }
}
