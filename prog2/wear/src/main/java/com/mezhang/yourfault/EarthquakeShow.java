package com.mezhang.yourfault;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

public class EarthquakeShow extends Activity implements SensorEventListener, View.OnClickListener {
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private GoogleApiClient mGoogleApiClient;
    double pre1=0;
    double pre2=0;
    double pre3=0;
    double longitude;
    double latitude = 0;
    String place = null;
    double mag = 0;
    int distance = 0;



    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        this.mGoogleApiClient.connect();
        place = this.getIntent().getStringExtra("place");
        longitude = this.getIntent().getDoubleExtra("longitude", 0);
        latitude = this.getIntent().getDoubleExtra("latitude", 0);
        mag = this.getIntent().getDoubleExtra("magnitude", 0);
        distance = this.getIntent().getIntExtra("distance", 0);
        ((TextView)(this.findViewById(R.id.textView))).setText(place);
        ((TextView)(this.findViewById(R.id.textView2))).setText(distance+ " km\n" + mag + " M");
        Context context = getApplicationContext();
        CharSequence text = "Vibrating!";
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
        this.findViewById(R.id.layout).setOnClickListener(this);
        vibrate();
    }

    private void vibrate(){
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        long[] vibrationPattern = {0, 500, 50, 300};
        //-1 - don't repeat
        final int indexInPatternToRepeat = -1;
        vibrator.vibrate(vibrationPattern, indexInPatternToRepeat);

    }



    private void getConnectionPhone(){
        new Thread( new Runnable() {
            @Override
            public void run() {
                CapabilityApi.GetCapabilityResult capResult =
                        Wearable.CapabilityApi.getCapability(
                                mGoogleApiClient, "shake",
                                CapabilityApi.FILTER_REACHABLE)
                                .await();
                for(Node node : capResult.getCapability().getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient, node.getId(), "/photo",  place.getBytes()).await();

                }
            }
        }).start();
    }


    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.

    }

    @Override
    public final void onSensorChanged(SensorEvent event) {


        if (pre1 + pre2 +pre3 < 1) {
            if (Math.abs(event.values[0]) + Math.abs(event.values[1]) + Math.abs(event.values[2]) > 1) {
                // Send message
                getConnectionPhone();

            }
        }
        pre1 = Math.abs(event.values[0]);
        pre2 = Math.abs(event.values[1]);
        pre3 = Math.abs(event.values[2]);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onClick(View v) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                CapabilityApi.GetCapabilityResult capResult =
                        Wearable.CapabilityApi.getCapability(
                                mGoogleApiClient, "shake",
                                CapabilityApi.FILTER_REACHABLE)
                                .await();
                for(Node node : capResult.getCapability().getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient, node.getId(), "/stop", new String(""+longitude+" "+latitude).getBytes()).await();

                }
            }
        }).start();
        this.finish();
    }
}