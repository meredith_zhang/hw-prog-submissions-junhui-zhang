package com.mezhang.youanimal;


import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Button;
import android.view.View;
import android.app.Activity;
import android.os.Bundle;



import java.util.Hashtable;

public class year extends Activity {

    private Hashtable<String,Double> animal_to_human_years = new Hashtable<String, Double>();

    private Spinner spinner1, spinner2;
    private EditText text1, text2;
    private Button button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year);

        createtable(animal_to_human_years);

        spinner1 = (Spinner) findViewById(R.id.spinner);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        text1 = (EditText) findViewById(R.id.editText);
        text2 = (EditText) findViewById(R.id.editText2);
        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String animal1 = spinner1.getSelectedItem().toString();
                String animal2 = spinner2.getSelectedItem().toString();
                String age = text1.getText().toString();
                try {
                    double inputAge = Double.valueOf(age);
                    double targetAge = convert(animal1, animal2, inputAge);
                    String output = "" + targetAge;
                    text2.setText(output);
                }catch (Exception e){

                }



            }
        });
    }


    public void createtable(Hashtable<String,Double> animal_to_human_years) {
        animal_to_human_years.put("Human", 1.0);
        animal_to_human_years.put("Bear(Oski)", 2.0);
        animal_to_human_years.put("Cat", 3.2);
        animal_to_human_years.put("Dog", 3.64);
        animal_to_human_years.put("Hamster", 20.0);
        animal_to_human_years.put("Hippopotamus", 1.78);
        animal_to_human_years.put("Kangaroo", 8.89);
        animal_to_human_years.put("Chicken", 5.33);
        animal_to_human_years.put("Cow", 3.64);
        animal_to_human_years.put("Deer", 2.29);
        animal_to_human_years.put("Duck", 4.21);
        animal_to_human_years.put("Elephant", 1.14);
        animal_to_human_years.put("Fox", 5.71);
        animal_to_human_years.put("Goat", 5.33);
        animal_to_human_years.put("Groundhog", 5.71);
        animal_to_human_years.put("Guinea pig", 10.0);
        animal_to_human_years.put("Horse", 2.0);
        animal_to_human_years.put("Lion", 2.29);
        animal_to_human_years.put("Monkey", 3.2);
        animal_to_human_years.put("Mouse", 20.0);
        animal_to_human_years.put("Parakeet", 4.44);
        animal_to_human_years.put("Pig", 3.2);
        animal_to_human_years.put("Pigeon", 7.27);
        animal_to_human_years.put("Rabbit", 8.89);
        animal_to_human_years.put("Rat", 26.67);
        animal_to_human_years.put("Sheep", 5.33);
        animal_to_human_years.put("Squirrel", 5.0);
        animal_to_human_years.put("Wolf", 4.44);






    }

    public double convert(String n1,String n2,double age){
        double mul = animal_to_human_years.get(n1);
        double dev = animal_to_human_years.get(n2);
        double targetAge = age*mul/dev;
        targetAge = Math.round(targetAge * 100);
        targetAge = targetAge/100;
        return targetAge;
    }






    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_year, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

}
